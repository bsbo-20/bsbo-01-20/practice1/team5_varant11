﻿using System;
using System.Collections.Generic;

namespace UML1
{
  class Program
  {
    static void Main(string[] args)
    {
      while (true)
      {
        Console.WriteLine("1. Добавить слово");
        Console.WriteLine("2. Печать файла");
        Console.WriteLine("3. Выход");
        Console.WriteLine("Введите номер действия");

        int ch = Convert.ToInt32(Console.ReadLine());
        Redactor redactor = new Redactor();
        if (ch == 1)
        {
          redactor.EditText("text");
          redactor.ShowText();
        }
        if (ch == 2)
        {
          redactor.Print();
        }
        if (ch == 3)
          break;
      }
    }
   }

  interface ICommand
  {
    public void Execute();
    public void Undo();
  }

  class AppendCommand : ICommand
  {
    string text;
    int wordIndex;
    Paragraph paragraph;
    public AppendCommand(Paragraph par, string text, int wordIndex)
    {
      this.text = text;
      this.wordIndex = wordIndex;
      this.paragraph = par;

    }
    public void Execute()
    {
      Console.WriteLine("AppendWords - AppendCommand");
      paragraph.AppendWords(text, wordIndex);

    }
    public void Undo()
    {

    }
  }


  class RemoveCommand : ICommand
  {

    int wordIndex;
    Paragraph paragraph;
    public RemoveCommand(Paragraph par, int wordIndex)
    {
      this.wordIndex = wordIndex;
      this.paragraph = par;
    }
    public void Execute()
    {
      paragraph.RemoveWords();
    }
    public void Undo()
    {

    }
  }


  class EditCommand : ICommand
  {
    string text;
    int wordIndex;
    Paragraph paragraph;
    public EditCommand(Paragraph par, string text, int wordIndex)
    {
      this.text = text;

      this.wordIndex = wordIndex;
      this.paragraph = par;

    }
    public void Execute()
    {
      paragraph.EditWords();
    }
    public void Undo()
    {

    }
  }
  enum commandType
  {
    append, remove, edit
  }

  
  class Redactor
  {
    File file;
    public Redactor()
    {
      file = new File();
    }
    void AddToStack()
    {
      Console.WriteLine("AddToStack");
    }
    void GoTo(int[] index)
    {
      Console.WriteLine("GoTo");
    }
    public void OpenFile(string path)
    {
      Console.WriteLine("OpenFile");
    }
    public void CreateFile(string path, string name)
    {
      Console.WriteLine("CreateFile");
    }
    public void Print()
    {
      Console.WriteLine("Print - Redactor");
      file.Print();
    }
    public void ShowText()
    {
      Console.WriteLine("ShowText");
    }
    public void Save()
    {

    }
    public void Undo()
    {

    }
    public void Search(string searchCom)
    {

    }
    public void Replace(string searchCom)
    {

    }
    public void EditText(string text)
    {
      Console.WriteLine("EditText - Redactor");
      file.EditText(text);
    }
  }


  class WordCorrector
  {
    string[] keys;
    string[] colorList;
    static WordCorrector wordCorr;
    public static WordCorrector GetWordCorrector()
    {
      if (wordCorr == null)
        wordCorr = new WordCorrector();
      return wordCorr;
    }
    public void HighLight(int parIndex, string type)
    {
      Console.WriteLine("HighLight");
    }
  }


  class File
  {
    string name;
    string path;
    string text;
    string fileType;
    public List<Paragraph> pars;
    Stack<ICommand> commandsHistory;
    public ICommand currentCommand;
    public File()
    {
      commandsHistory = new Stack<ICommand>();
      pars = new List<Paragraph>();
      pars.Add(new Paragraph());
    }
    public void Print()
    {
      Console.WriteLine("Print - File");
      List<Paragraph> paragraphs = GetParagraphs();
      foreach (Paragraph paragraph in paragraphs)
        paragraph.GetProps();
      Console.WriteLine("Печать");
    }
    public List<Paragraph> GetParagraphs()
    {
      Console.WriteLine("GetParagraphs - File");
      return pars;
    }
    public void GetProp(List<Paragraph> par)
    {
      Console.WriteLine("GetProp - File");
    }
    void SetText()
    {

    }


    int GetParIndex()
    {
      Console.WriteLine("GetParIndex - File");
      return 0;
    }
    int GetWordIndex()
    {
      Console.WriteLine("GetWordIndex - File");
      return 0;
    }
    commandType GetInputType()
    {
      Console.WriteLine("GetInputType - File");
      return commandType.append;
    }
    public void EditText(string text)
    {
      Console.WriteLine("EditText - File");
      commandType c = GetInputType();
      int parIndex = GetParIndex();
      int wordIndex = GetWordIndex();
      if (c == commandType.append)
      {
        currentCommand = new AppendCommand(pars[parIndex], text, wordIndex);

      }
      if (c == commandType.remove)
      {
        currentCommand = new RemoveCommand(pars[parIndex], wordIndex);

      }
      if (c == commandType.edit)
      {
        currentCommand = new EditCommand(pars[parIndex], text, wordIndex);
      }
      currentCommand.Execute();
      WordCorrector.GetWordCorrector().HighLight(parIndex, fileType);
      commandsHistory.Push(currentCommand);
    }

  }

  class Paragraph
  {
    public List<Word> words;
    string parText;
    public Paragraph()
    {
      words = new List<Word>();
    }
    public List<Word> GetWords(string text)
    {
      Console.WriteLine("GetWords - Paragraph");
      return new List<Word>();
    }
    public void GetProps()
    {

      Console.WriteLine("GetProps - Paragraph");
    }

    public void AppendWords(string text, int wordIndex)
    {
      Console.WriteLine("AppendWords - Paragraph");
      words.InsertRange(wordIndex, (GetWords(text)));
    }
    public void RemoveWords()
    {

    }
    public void EditWords()
    {

    }
  }

  class Word
  {
    public string word;
    public string font;
    public int size;
    public string color;
    public Word(string word, string color = null, string font = "Times ...", int size = 12)
    {
      Console.WriteLine("Cons - Word");
      this.word = word;
      this.font = font;
      this.size = size;
      this.color = color;
    }
  }



}


